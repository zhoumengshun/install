const { Step } = antd.Steps;
const { current } = 1;


const steps = [
    {
        title: '许可协议',
        content: 'First-content',
    },
    {
        title: '环境检查',
        content: 'Second-content',
    },
    {
        title: '基础服务安装',
        content: 'Last-content',
    },
    {
        title: '完成',
        content: 'Last-content',
    },
];

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            current: 1,
        };
    }

    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }


    render() {
        const { current } = this.state;
        return (
                <div>
                    <antd.Steps current={current}>
                        {steps.map(item => (
                                <Step  key={item.title} title={item.title} />
                        ))}

                     </antd.Steps>

                </div>

        );
    }

}
ReactDOM.render(
<App />, document.getElementById('steps'));