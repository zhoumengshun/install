package main

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/beego/i18n"
	"install/models"
	_ "install/routers"
	"os"
	"os/exec"
	"runtime"
)

var commands = map[string]string{
	"windows": "start",
	"darwin":  "open",
	"linux":   "xdg-open",
}
func Open(uri string) error {
	run, ok := commands[runtime.GOOS]
	if !ok && runtime.GOOS !="linux"{
		return fmt.Errorf("don't know how to open things on %s platform", runtime.GOOS)
	}
	beego.Debug(runtime.GOOS)
	cmd := exec.Command(run, uri)
	beego.Debug(cmd)
	return cmd.Start()
}
//判断文件是否存在
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}


const (
	APP_VER = "0.1.1.0227"
)



func main() {

	//判断是否已经安装过,安装逻辑
	installed,_ :=PathExists("conf/install.bak")
	if installed ==false{

		beego.Info(beego.BConfig.AppName, APP_VER)

		// Register template functions.
		beego.AddFuncMap("i18n", i18n.Tr)
		Open("http://127.0.0.1:5201")
		beego.Run()
	  	return
	}else{
		beego.Debug("install is alredy  exist")
		//安装过 启动docker
		containerName := "uniappadmin"
		containerId,_:=models.FindContainerIdByname(containerName)
		if containerId !=""{
			models.StartContainers(containerId)
		}
	}



	//已经安装过,启动逻辑
	//models.Init()


	//go build -ldflags "-H windowsgui" 后台运行
	//Open("http://127.0.0.1:5200")
}

